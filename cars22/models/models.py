# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import ValidationError
from datetime import timedelta


class Car(models.Model):
    _name = 'cars.cars'
    _description = 'Car Management '

    name = fields.Char(string="Name")
    horse_power = fields.Integer(string="Hourse Power")
    door_numbers = fields.Integer(string="Door Number")
    driver_id = fields.Many2one('res.partner', string="Driver ID")
    parking_id = fields.Many2one('parking.parking', string="Parking")
    status = fields.Selection([('new', 'New'), ('used', 'Used'), ('sold', 'Sold')], string="Status", default="new")
    car_sequence = fields.Char(string="Car Sequence")
    features_ids = fields.Many2many('feature.feature', string="Features")

    total_speed = fields.Integer('Speed Total', compute="get_total_speed")


    def get_total_speed(self):
        self.total_speed = self.horse_power * 10

    def set_car_to_used(self):
        self.status = 'used'

    def set_car_to_sold(self):
        self.status = 'sold'
        template_id = self.env.ref('cars.car_mail_template')
        if template_id:
            template_id.send_mail(self.id, force_send=True, raise_exception=True,
                                  email_values={
                                      'email_to': self.driver_id.email
                                  })

    @api.model
    def create(self, vals):
        vals['car_sequence'] = self.env['ir.sequence'].next_by_code('car.sequence')

        if vals['door_numbers'] == 6:
            raise ValidationError("Max Number for Car Doors is 6")

        result = super(Car, self).create(vals)
        return result

    def unlink(self):
        for rec in self:
            if rec.horse_power == 5:
                raise ValidationError("Horse Power Cannot be 5")
            return super(Car, self).unlink()
    #     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         for record in self:
#             record.value2 = float(record.value) / 100
