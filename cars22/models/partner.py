# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Partner(models.Model):
    _inherit = 'res.partner'
    #     _description = 'my_first_module.my_first_module'
    mynote1 = fields.Char(string='Custome MyNote 1')
    car_count = fields.Integer(string="Car Count", compute='get_car_number')

    #

    def get_car_number(self):
        for rec in self:
            rec.car_count = self.env['fleet.vehicle'].search_count([('driver_id', '=', self.id)])

    def get_cars(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window',
            'name': 'Cars',
            'view_mode': 'tree,form',
            'res_model': 'fleet.vehicle',
            'domain': [('driver_id', '=', self.id)],
            # 'context': "{'create':False}"
        }
