# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Parking(models.Model):
    _name = 'parking.parking'
    _description = 'Parking Car Managment'

    name = fields.Char(string="Name")
    car_ids = fields.One2many('cars.cars', 'parking_id')

#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         for record in self:
#             record.value2 = float(record.value) / 100
