# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Feature(models.Model):
    _name = 'feature.feature'
    _description = 'Feature Car Management'

    name = fields.Char(string="Name")

    value = fields.Char(string='value')

#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         for record in self:
#             record.value2 = float(record.value) / 100
