# -*- coding: utf-8 -*-

from odoo import models, fields, api


class cars(models.Model):
    _name = 'cars.cars'
    _description = 'cars.cars'

    name = fields.Char(string='Name')
    horse_power = fields.Integer(string="Horse Power")
    door_numbers = fields.Integer(string="Doors Numbers")
    driver = fields.Many2one('res.partner', string="Driver")
    start_date = fields.Date(string="Date")
    is_sport = fields.Boolean('Is Sport ? ')
    file = fields.Binary("File")

#
#     @api.depends('value')
#     def _value_pc(self):
#         for record in self:
#             record.value2 = float(record.value) / 100
