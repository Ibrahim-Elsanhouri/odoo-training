from odoo import http
from odoo.http import request
import base64


class Cars(http.Controller):
    @http.route('/first', auth='public')
    def index(self, **kw):
        return "Hello, world from my first route"

    @http.route('/cars', auth='public', website=True)
    def display_cars(self, **kw):
        cars = http.request.env['cars.cars']
        return http.request.render('cars.display_cars', {
            'cars': cars.search([])

        })

    @http.route('/cars/create', auth='public', website=True)
    def redirect_to_form_car_create(self, **kw):
        return request.render('cars.create_car_form')

    @http.route('/create', auth='public', website=True)
    def create_car_form(self, **kw):
        if kw['file']:
            file = kw.get('file').stream.read()

        request.env['cars.cars'].create({
        'name': kw.get('name'),
        'horse_power': kw.get('horse_power'),
        'door_numbers': kw.get('door_numbers'),
        'driver': kw.get('driver'),
        'start_date': kw.get('start_date'),
        'is_sport': True if kw.get('is_sport') else False,
        'file': base64.encodestring(kw.get('file'))

         })
        return http.request.redirect('/cars')


@http.route('/update', auth='public', website=True)
def redirect_to_update_form(self, **kw):
    print(kw.get('id'))
    vals = {}
    car_object = request.env['cars.cars'].search([('id', '=', kw.get('id'))])
    vals.update({
        'car': car_object

    }
    )
    return http.request.render('cars.update_car_form', vals)


@http.route('/update/car', auth='public', website=True)
def update_car(self, **kw):
    print(kw.get('driver'))

    http.request.env['cars.cars'].search([('id', '=', kw.get('id'))]).write({
        'name': kw.get('name'),
        'horse_power': kw.get('horse_power'),
        'door_numbers': kw.get('door_numbers'),

    })
    return http.request.redirect('/cars')


@http.route('/delete', auth='public', website=True)
def update_car(self, **kw):
    http.request.env['cars.cars'].search([('id', '=', kw.get('id'))]).unlink()
    return http.request.redirect('/cars')

#         return http.request.render('cars.listing', {
#             'root': '/cars/cars',
#             'objects': http.request.env['cars.cars'].search([]),
#         })

#     @http.route('/cars/cars/objects/<model("cars.cars"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('cars.object', {
#             'object': obj
#         })
